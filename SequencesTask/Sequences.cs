﻿using System;
using System.Threading;

namespace SequencesTask
{
    public static class Sequences
    {
#pragma warning disable
        public static string[] GetSubstrings(string numbers, int length)
        {
            long k;
            long.TryParse(numbers, out k);

            if (string.IsNullOrWhiteSpace(numbers) || numbers.Length == 0 || k == 0)
            {
                throw new ArgumentException(null, nameof(numbers));
            }

            if (length <= 0 || length > numbers.Length)
            {
                throw new ArgumentException(null, nameof(length));
            }

            string[] substrings = new string[numbers.Length - length + 1];

            for (int i = 0; i <= numbers.Length - length; i++)
            {
                substrings[i] = numbers.Substring(i, length);
            }

            return substrings;
        }
    }
}
